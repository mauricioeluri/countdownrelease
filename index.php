<?php
$nomeEmpresa = "Demo";
$anoLancamento = 2018;
$mesLancamento = 1;
$diaLancamento = 1;
$horaLancamento = 0;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Novo site em desenvolvimento">
        <meta name="author" content="Maurício El Uri">
        <title><?= $nomeEmpresa . " - novo site em desenvolvimento!" ?></title>
        <script src="arquivos/countdown.min.js" type="text/javascript"></script>
        <script>
            window.onload = function () {
                var clock = document.getElementById("countdown")
                        , targetDate = new Date(<?= "$anoLancamento, " . ($mesLancamento - 1) . ", $diaLancamento, $horaLancamento" ?>);

                setInterval(function () {
                    countdown.setLabels(
                            ' milissegundo| segundo| minuto| hora| dia| semana| mês| ano| década| século| milênio',
                            ' milissegundos| segundos| minutos| horas| dias| semanas| meses| anos| décadas| séculos| milênios',
                            ' e ',
                            ', ',
                            'agora');
                    clock.innerHTML = countdown(targetDate).toString();
                }, 1000);
            };
        </script>
        <link href="arquivos/bootstrap.css" rel="stylesheet">
        <link href="arquivos/bootstrap-theme.css" rel="stylesheet">
        <link href="arquivos/style.css" rel="stylesheet">
    </head>

    <body>
        <div id="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?= $nomeEmpresa ?></h1>
                        <h2 class="subtitle">Estamos trabalhando em um novo site!<br/>Lançamento em:</h2>
                        <div id="countdown"></div>
                        <!--                        <form class="form-inline signup" role="form">
                                                    <div class="form-group">
                                                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter your email address">
                                                    </div>
                                                    <button type="submit" class="btn btn-theme">Get notified!</button>
                                                </form>-->
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer">
                <div class="col-lg-6 col-lg-offset-3">
                    <p class="copyright">&copy; <?= $nomeEmpresa ?> - Todos os direitos reservados</p>
                </div>
            </footer>
    </body>
    
</html>
